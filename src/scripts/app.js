const form = document.querySelector('.form');
const formInputs = form.querySelectorAll('input');
const submitButton = form.querySelector('.submit');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    formInputs.forEach(el => {
        console.log(`${el.getAttribute('name')}: ${el.value}`);
    })
});
